

var uls= document.getElementsByTagName('ul')
var ps= document.getElementsByClassName('p')
var swiper = document.getElementById('swiper')
var swiperContainer = document.getElementById('swiper-container')
var H = document.documentElement.clientHeight;
swiper.style.height = H+'px'
for(var i = 0; i < ps.length; i++){
	ps[i].style.height = H+'px'
}


var startY, moveY, pageNum=1, scrollTimer=null;
swiper.ontouchstart = function(e){
	startY = e.touches[0].clientY
	if(scrollTimer){
		clearTimeout(scrollTimer)
		scrollTimer = null
		go(swiperContainer, -H*pageNum)
	}else{
		swiper.ontouchend = function(e){
			console.log(pageNum)
			if(startY > moveY && uls[pageNum-1].offsetHeight <= H){
				go(swiperContainer, -H*pageNum)
			}
		}
	}
}
swiper.ontouchmove = function(e){
	moveY = e.touches[0].clientY
}
swiper.ontouchend = function(e){
	console.log(pageNum)
	if(startY > moveY && uls[pageNum-1].offsetHeight <= H){
		go(swiperContainer, -H*pageNum)
	}
}

for(let i = 0; i < uls.length; i++){
		(function(n){
			ps[n].onscroll=function(){
				if( uls[n].offsetHeight > H){
					swiper.ontouchend = null
					var num = n+1> ps.length? ps.length-1:n+1
					endScroll(this, num)
				}
			}
		})(i)
}


function endScroll(obj, num){
	clearTimeout(scrollTimer)
	if(obj.scrollHeight-obj.scrollTop == obj.clientHeight){
		scrollTimer = setTimeout(function(){
			go(swiperContainer, -H*num)
		},100000)
	}
}





function go(obj,target){
	clearInterval(obj.timer);

	pageNum++;
	if (pageNum> ps.length) return;

	var speed = obj.offsetTop < target? 10 : -10;
	obj.timer=setInterval(function(){
			var result = obj.offsetTop - target;
			obj.style.top= obj.offsetTop + speed +"px"
			if(Math.abs(result) <= 10){
					clearInterval(obj.timer);
					obj.style.top = target +"px"
					swiper.ontouchend = function(e){
						if(startY > moveY && uls[pageNum-1].offsetHeight <= H){
							go(swiperContainer, -H*pageNum)
						}
					}
			}
	},10)
}
