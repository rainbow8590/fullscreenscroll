 var overscroll = function(el) {
	el.addEventListener('touchstart', function() {
			var top = el.scrollTop
			,totalScroll = el.scrollHeight
			,currentScroll = top + el.offsetHeight;
			if(top === 0) {
					el.scrollTop = 1;
			}else if(currentScroll === totalScroll) {
					el.scrollTop = top - 1;
			}
	});

	el.addEventListener('touchmove', function(evt) {
	if(el.offsetHeight < el.scrollHeight)
			evt._isScroller = true;
	});
}
			
overscroll(document.querySelector('.swiper')); 
document.body.addEventListener('touchmove', function(evt) {
	console.log(evt._isScroller)
	if(!evt._isScroller) {
			evt.preventDefault();
	}
});

var swiperItemInners= document.getElementsByClassName('swiper-item-inner')
var swiperItems = document.getElementsByClassName('swiper-item')
var swiper = document.getElementById('swiper')
var swiperContainer = document.getElementById('swiper-container')
var test = document.getElementById('test')
var H = document.documentElement.clientHeight;
swiper.style.height = H+'px'
for(var i = 0; i < swiperItems.length; i++){
	swiperItems[i].style.height = H+'px'
}
// swiperItems[1].style.height = 945+'px'
swiper.onclick = function(){
	console.log('click')
	return;
}
var touchend = function (){
	console.log('end')
	console.log(startY , moveY)
	if(startY > moveY&&moveY != 0){
		if (pageNum>= swiperItems.length-1) return;
		swiperItems[pageNum].scrollTop = 0;
		pageNum++;
	}else if(startY < moveY){
		console.log(pageNum)
		swiperItems[pageNum].scrollTop = 0;
		if (pageNum<=0) return;
		pageNum--;
	}
	startY = moveY = 0
	go(swiperContainer, -H*pageNum)
}

var startY, moveY, pageNum=0, scrollTimer=null;
swiper.ontouchstart = function(e){
	console.log('start')
	startY = e.touches[0].clientY
	if(scrollTimer){
		clearTimeout(scrollTimer)
		scrollTimer = null
		go(swiperContainer, -H*pageNum)
	}else{
		swiper.ontouchend = touchend
	}
}
swiper.ontouchmove = function(e){
	// console.log(e)
	moveY = e.touches[0].clientY
	test.innerHTML = swiperItems[pageNum].scrollTop
	if(moveY > startY && swiperItems[pageNum].scrollTop <= 10){
		e.preventDefault()
	}
	if(moveY < startY &&  swiperItems[pageNum].scrollHeight- swiperItems[pageNum].scrollTop ==  swiperItems[pageNum].clientHeight){
		e.preventDefault()
	}
}
swiper.ontouchend = touchend

for(let i = 0; i < swiperItems.length; i++){
	(function(n){
		swiperItems[n].onscroll=function(){
			if( swiperItemInners[n].offsetHeight > H){
				swiper.ontouchend = null
				var num = n+1> swiperItems.length? swiperItems.length-1:n+1
				endScroll(this, num)
			}
		}
	})(i)
}

function endScroll(obj, num){
	clearTimeout(scrollTimer)
	if(obj.scrollHeight-obj.scrollTop == obj.clientHeight){
		scrollTimer = setTimeout(function(){
			go(swiperContainer, -H*(num+1))
		},1000000)
	}else if(obj.scrollTop <= 0){
		scrollTimer = setTimeout(function(){
			go(swiperContainer, -H*(num-1))
		},1000000)
	}
}


function go(obj,target){
	clearInterval(obj.timer);
	// console.log(pageNum , obj.offsetTop , target)
	var speed = obj.offsetTop < target? 10 : -10;
	obj.timer=setInterval(function(){
			var result = obj.offsetTop - target;
			obj.style.top= obj.offsetTop + speed +"px"
			if(Math.abs(result) <= 10){
					clearInterval(obj.timer);
					obj.style.top = target +"px"
					swiper.ontouchend = touchend
			}
	},10)
}
